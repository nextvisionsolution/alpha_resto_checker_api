<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Closing_Date extends MY_Model {

    protected $table = 'CLOSINGDATE';

    public function getUserCloseKosong()
    {
        $this->db->where('USERCLOSE', '');
        $query = $this->db->get($this->table);

        return $query->row();
    }

}
