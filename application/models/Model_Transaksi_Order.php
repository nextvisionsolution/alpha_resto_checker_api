<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Transaksi_Order extends MY_Model {

    protected $table = 'TORDER';

    public function getTransaksiBelumDitutup()
    {
        $query = $this->db->query('SELECT A.KODETRANS FROM TORDER A INNER JOIN CLOSINGDATE B ON A.tglomzet = B.tglomzet 
            WHERE B.userclose = \'\' AND A.status = \'P\'');
        return $query->result();
    }
	
	public function getUser($user,$pass)
    {
        $query = $this->db->query("SELECT a.USERNAME FROM MUSERFRONT A 
        INNER JOIN MHAKAKSESFRONT B ON A.KODEAKSES = B.KODEAKSES 
        WHERE A.PASS = '{$pass}' AND B.SYSTEM_CHECKER = 1 ");
		//A.USERNAME = '".$user."' AND
        return $query->row()->USERNAME;
    }

    public function getDaftarBill()
    {
		
		$sql = "SELECT A. TGLOMZET, A.KODETRANS, A.NOMORMEJA, A.JUMLAHORANG, A.WAITER, A.KODELOKASI,A.JAMTRANS, 
		CAST((CURRENT_TIME-A.JAMTRANS) AS INTEGER) AS MENITORDER, 
                       '' AS JMLMENU, '' AS JMLITEM, '' as DETAIL
                FROM TORDER A INNER JOIN TORDERDTL B ON A.KODETRANS=B.KODETRANS AND A.TGLOMZET=B.TGLOMZET
                     LEFT JOIN CLOSINGDATE C ON C.TGLOMZET=A.TGLOMZET
                WHERE C.USERCLOSE = '' 
                GROUP BY A.TGLOMZET, A.JAMTRANS, A.KODETRANS, A.NOMORMEJA, A.JUMLAHORANG, A.WAITER, A.KODELOKASI
				ORDER BY A.TGLOMZET DESC, A.JAMTRANS DESC, A.NOMORMEJA"; //and a.KODETRANS = '122209'
				
        $query = $this->db->query($sql)->result();
		
		foreach($query as $rows){
			
			$sql = "SELECT A.KODETRANS,(COUNT(DISTINCT(A.KODEMENURECIPE))) AS JMLMENU, CAST(SUM(A.JML)-SUM(A.JMLVOID) AS INTEGER) as JMLITEM
                FROM TORDERDTL A
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				and b.CHECKER = 1 
				AND A.KODETRANS = '".$rows->KODETRANS."'
				AND A.KODELOKASI = '".$rows->KODELOKASI."'  
				AND A.JENIS='' 
				AND A.SPESIFIKASI = 'REGULAR' 
				AND (a.JML-a.JMLVOID) > 0
				GROUP BY A.KODETRANS";
                       
			$jml = $this->db->query($sql)->row();
			
			if($jml->JMLMENU == "")
			{
				$rows->JMLMENU 	= 0;
			}
			else
			{
				$rows->JMLMENU 	= $jml->JMLMENU;
			}
			
			if($jml->JMLITEM == "")
			{
				$rows->JMLITEM 	= 0;
			}
			else
			{
				$rows->JMLITEM 	= $jml->JMLITEM;
			}
			
			$sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as ITEM
                FROM TORDERCHECKER A
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
                 WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				 AND A.KODETRANS = '".$rows->KODETRANS."' 
				 AND B.CHECKER = 1  
				 AND A.KODELOKASI = '".$rows->KODELOKASI."'";
                       
			$item = $this->db->query($sql)->row();
			
			
			if($item->JMLITEM == "")
			{
				$item->JMLITEM 	= 0;
			}
			
			$rows->JMLITEM 	= $rows->JMLITEM - $item->ITEM;
			
			$sql = "SELECT A.KODETRANS,A.NAMAFRONT,A.KODEMENURECIPE,SUM(a.JML-a.JMLVOID) as JML, '' as MODIFIER
                FROM TORDERDTL A
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				and b.CHECKER = 1 
				AND A.KODETRANS = '".$rows->KODETRANS."'
				AND A.KODELOKASI = '".$rows->KODELOKASI."'  
				AND A.JENIS='' 
				AND A.SPESIFIKASI = 'REGULAR' 
				AND (a.JML-a.JMLVOID) > 0
				GROUP BY A.KODETRANS,A.NAMAFRONT,A.KODEMENURECIPE";
                       
			$resultMenu   = $this->db->query($sql)->result();
			foreach($resultMenu as $rowsMenu){
				
				$sqlMod = "SELECT LIST(A.NAMAFRONT) as NAMAFRONT
					FROM TORDERDTL A
					INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
					WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
					AND b.CHECKER = 1 
					AND A.KODETRANS = '".$rows->KODETRANS."'
					AND A.KODELOKASI = '".$rows->KODELOKASI."'  
					AND A.KODEMENURECIPE = '".$rowsMenu->KODEMENURECIPE."'  
					AND A.SPESIFIKASI = 'MODIFIER'";
                       
				$resultModifier   = $this->db->query($sqlMod)->result();
				
				foreach($resultModifier as $rowsMod){
					$rowsMenu->MODIFIER = $rowsMod->NAMAFRONT??'';
				}
				
			}
			$rows->DETAIL = $resultMenu;
			
		}
		//CEK NOL TIDAK KELUAR DI FRONT

        return $query;
    }
	
	public function getDaftarMenu()
    {
		
        $sql = "SELECT b.jamtrans,a.NAMA as KETERANGAN,a.NAMAFRONT, a.KODE as KODEMENURECIPE, C.NOMORMEJA,c.TGLTRANS,c.TGLOMZET,c.KODELOKASI,c.KODETRANS, B.JMLVOID,
		CAST((B.JML - B.JMLVOID) AS INTEGER) as JML, CAST((B.JML - B.JMLVOID) AS INTEGER) as SISA, CAST((IIF(b.DURASI is null,0,b.DURASI) - FLOOR((CURRENT_TIME-B.JAMTRANS)/60)) AS INTEGER) AS JUMLAHMENIT,b.URUTAN,IIF(b.DURASI is null,0,b.DURASI) as DURASI
        FROM MMENURECIPE A
            INNER JOIN TORDERDTL B ON A.KODE=B.KODEMENURECIPE 
            INNER JOIN TORDER C ON C.KODETRANS = B.KODETRANS AND C.TGLOMZET=B.TGLOMZET
            INNER JOIN CLOSINGDATE D ON C.TGLOMZET=D.TGLOMZET
        WHERE B.SPESIFIKASI = 'REGULAR' AND D.USERCLOSE = '' AND (b.JML-b.JMLVOID) > 0 AND a.CHECKER = 1
        ORDER BY a.NAMA,C.TGLOMZET DESC, C.JAMTRANS DESC, C.KODETRANS ASC
                ";
                       
        $query = $this->db->query($sql)->result();
		
		foreach($query as $rows){
			
			$sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as SISA
                FROM TORDERCHECKER A
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				AND A.KODETRANS = '".$rows->KODETRANS."' 
				AND A.KODELOKASI = '".$rows->KODELOKASI."'
				AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."'";
                       
			$sisa = $this->db->query($sql)->row();
			
			
			if($sisa->SISA == "")
			{
				$sisa->SISA 	= 0;
			}
			
			$rows->SISA 	= $rows->SISA - $sisa->SISA;
			
			$sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as ITEM
                FROM TORDERKITCHEN A
                 WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				 AND A.KODETRANS = '".$rows->KODETRANS."'  
				 AND A.KODELOKASI = '".$rows->KODELOKASI."' 
				 AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."'";
                       
			$item = $this->db->query($sql)->row();
			
			if($item->ITEM == "")
			{
				$item->ITEM 	= 0;
			}
			
			$rows->SISA 	= $rows->SISA - $item->ITEM;
			
		}
        return $query;
    }
	
	public function getDaftarFinish()
    {
        $sql = "SELECT A.TGLOMZET, A.KODETRANS, A1.NOMORMEJA,
				'' as JMLMENU, '' as JMLITEM,A1.JUMLAHORANG,'' as JAMFINISH,A.KODELOKASI
                FROM TORDERCHECKER A
                     INNER JOIN  TORDER A1 on A1.KODETRANS = A.KODETRANS and A1.TGLOMZET = A.TGLOMZET
					 INNER JOIN TORDERDTL B ON A1.KODETRANS = B.KODETRANS AND A1.TGLOMZET = B.TGLOMZET AND A.KODEMENURECIPE = B.KODEMENURECIPE 
                     INNER JOIN CLOSINGDATE C ON C.TGLOMZET=A1.TGLOMZET
                WHERE B.SPESIFIKASI = 'REGULAR' AND C.USERCLOSE = '' AND (b.JML-b.JMLVOID) > 0
                GROUP BY A.TGLOMZET, A.KODETRANS, A1.NOMORMEJA, A1.JUMLAHORANG,A.KODELOKASI
                ORDER BY A.TGLOMZET DESC, A.KODETRANS DESC, A1.NOMORMEJA, A1.JUMLAHORANG,A.KODELOKASI
                ";
		$query = $this->db->query($sql)->result();	
		
		foreach($query as $rows){
			
			 $sql = "SELECT MAX(A.JAMFINISH) as JAMFINISH,A.KODETRANS,(COUNT(DISTINCT(A.KODEMENURECIPE))) AS JMLMENU, CAST(SUM(A.JUMLAH) AS INTEGER) as JMLITEM
                FROM TORDERCHECKER A
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				AND A.KODETRANS = '".$rows->KODETRANS."' 
				AND A.KODELOKASI = '".$rows->KODELOKASI."' 
				GROUP BY A.KODETRANS";
                       
			$jml = $this->db->query($sql)->row();
			
			if($jml->JMLMENU == "")
			{
				$rows->JMLMENU 	= 0;
			}
			else
			{
				$rows->JMLMENU 	= $jml->JMLMENU;
			}
			
			if($jml->JMLITEM == "")
			{
				$rows->JMLITEM 	= 0;
			}
			else
			{
				$rows->JMLITEM 	= $jml->JMLITEM;
			}
				
			$rows->JAMFINISH 	= $jml->JAMFINISH;
		}
                       
      
        return $query;
    }
	
	public function getDaftarDetailTambahan($kodemenu)
    {
		 $sql = "SELECT A.NAMAFRONT, C.NOMORMEJA as MEJA, CAST(B.JML as INTEGER) as JML , B.KETERANGAN
        FROM MMENURECIPE A
            INNER JOIN TORDERDTL B ON A.KODE=B.KODEMENURECIPE 
            INNER JOIN TORDER C ON C.KODETRANS = B.KODETRANS AND C.TGLOMZET=B.TGLOMZET
            INNER JOIN CLOSINGDATE D ON C.TGLOMZET=D.TGLOMZET
        WHERE a.PREPARE = 1 and B.SPESIFIKASI = 'MODIFIER' 
		AND A.KODE = '$kodemenu' 
		AND D.USERCLOSE = '' 
		AND (b.JML-b.JMLVOID) > 0
		";
        $query = $this->db->query($sql)->result();          
		
        return $query;
	}
	
	public function getDaftarKitchen()
    {
		
        $sql = "SELECT b.jamtrans,a.NAMA as KETERANGAN,a.NAMAFRONT, a.KODE as KODEMENURECIPE, C.NOMORMEJA,c.TGLTRANS,c.TGLOMZET,c.KODELOKASI,c.KODETRANS, B.JMLVOID,
		CAST((B.JML - B.JMLVOID) AS INTEGER) as JML, CAST((B.JML - B.JMLVOID) AS INTEGER) as SISA
        FROM MMENURECIPE A
            INNER JOIN TORDERDTL B ON A.KODE=B.KODEMENURECIPE 
            INNER JOIN TORDER C ON C.KODETRANS = B.KODETRANS AND C.TGLOMZET=B.TGLOMZET
            INNER JOIN CLOSINGDATE D ON C.TGLOMZET=D.TGLOMZET
        WHERE a.PREPARE = 1 and B.SPESIFIKASI = 'REGULAR' AND D.USERCLOSE = '' AND (b.JML-b.JMLVOID) > 0
		
	   ORDER BY a.NAMA,C.TGLOMZET DESC, C.JAMTRANS DESC, C.KODETRANS ASC
                ";//AND A.KODE = '1996-0001'       
                       
        $query = $this->db->query($sql)->result();
		$oldnama = "";
		foreach($query as $rows){
			if($oldnama != $rows->KETERANGAN){
				
				$sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as JML
					FROM TORDERKITCHEN A
					WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
					AND A.KODELOKASI = '".$rows->KODELOKASI."'
					AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."'";
						   
				$jml = $this->db->query($sql)->row();
				
				if($jml->JML == "")
				{
					$jml->JML 	= 0;
				}

				$rows->JML 		= $rows->JML-$jml->JML;
				$rows->SISA 	= $rows->SISA-$jml->JML;
				
				$oldnama = $rows->KETERANGAN;
			}
		}
        return $query;
    }
	
	
	
	public function getDaftarKitchenFinish()
    {
		
        $sql = "SELECT a.NAMA as KETERANGAN,a.NAMAFRONT,A.KODE AS KODEMENURECIPE,
		CAST((B.JUMLAH) AS INTEGER) as JML, CAST((B.JUMLAH) AS INTEGER) as SISA 
        FROM MMENURECIPE A
            INNER JOIN TORDERKITCHEN B ON A.KODE=B.KODEMENURECIPE 
            INNER JOIN CLOSINGDATE D ON B.TGLOMZET=D.TGLOMZET
        WHERE a.PREPARE = 1 AND D.USERCLOSE = '' 
	    ORDER BY a.NAMA ASC,D.TGLOMZET DESC, A.KODE ASC
                "; //AND A.KODE = '1996-0001'       
                       
        $query = $this->db->query($sql)->result();
		
        return $query;
    }
	
    public function getDaftarDetailMenu($tglomzet, $kodetrans)
    {

        $sql = "SELECT a.*, CAST(a.JML-a.JMLVOID AS INTEGER) AS JML2, CAST(a.JML-a.JMLVOID AS INTEGER) AS SISA, ((IIF(a.DURASI is null,0,a.DURASI) - (CURRENT_TIME-a.JAMTRANS)/60)) AS JUMLAHMENIT,a.URUTAN,IIF(a.DURASI is null,0,a.DURASI) as DURASI,a.URUTAN
                FROM TORDERDTL a
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
                WHERE A.TGLOMZET='".$tglomzet."' AND A.KODETRANS = '".$kodetrans."' AND A.JENIS='' AND (a.JML-a.JMLVOID) > 0 AND a.SPESIFIKASI = 'REGULAR' AND B.CHECKER = 1
				ORDER BY a.TGLTRANS,a.JAMTRANS,a.URUTAN";
        
        $query = $this->db->query($sql)->result();
		
		foreach($query as $rows){
			
			 $sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as SISA
                FROM TORDERCHECKER A
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				AND A.KODETRANS = '".$rows->KODETRANS."' 
				AND A.KODELOKASI = '".$rows->KODELOKASI."'
				AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."' 
				AND A.JAMORDER = '".$rows->JAMTRANS."' 
				AND a.URUTAN = $rows->URUTAN";
                       
			$sisa = $this->db->query($sql)->row();
			
			if($sisa->SISA == "")
			{
				$sisa->SISA 	= 0;
			}
			$rows->SISA 		= $rows->SISA-$sisa->SISA;
			
		}
		
        return $query;
    }
	
	public function getDaftarDetailFinish($tglomzet, $kodetrans)
    {
        $sql = "SELECT DISTINCT(A.KODEMENURECIPE) as KODEMENURECIPE,a.TGLOMZET,a.NAMAMENURECIPE,c.NOMORMEJA,a.KODETRANS, '0' as JML2,a.URUTAN,b.NAMAFRONT, C.JUMLAHORANG,a.KODELOKASI,a.JAMORDER
                FROM TORDERCHECKER A
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = b.KODE
				INNER join TORDER C on C.KODETRANS = A.KODETRANS AND C.TGLOMZET = A.TGLOMZET
                WHERE A.TGLOMZET='".$tglomzet."' AND A.KODETRANS = '".$kodetrans."'				
		";
        
        $query = $this->db->query($sql)->result();
		
		foreach($query as $rows){
			
			 $sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as JML2
                FROM TORDERCHECKER A
                WHERE A.TGLOMZET='".$rows->TGLOMZET."' 
				AND A.KODETRANS = '".$rows->KODETRANS."' 
				AND a.KODEMENURECIPE = '".$rows->KODEMENURECIPE."' 
				AND A.JAMORDER = '".$rows->JAMORDER."'
				AND A.KODELOKASI = '".$rows->KODELOKASI."'
				AND a.URUTAN = $rows->URUTAN";
                       
			$jml = $this->db->query($sql)->row();
			
			if($jml->JML2 == ""){
				$jml->JML2 = 0;
			}
			$rows->JML2 		= $jml->JML2;
		}

        return $query;
    }
	
	public function simpanDetailOrder($data_menu, $data_jmlout, $data_sisa, $data_macaddress,$data_username)
    {

		$i = 0;
		
		//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
		$sql = "SELECT URUTANCHECKER,CURRENT_TIME as JAMNOW FROM TORDERCHECKER
			ORDER BY URUTANCHECKER DESC";
			
		$urutanchecker = $this->db->query($sql)->row()->URUTANCHECKER;
		
		//UNTUK AMBIL JAM DISERVER
		$sqlJam = "SELECT CURRENT_TIME as JAMNOW FROM TORDERDTL";
		
		$tglFinish = date('Y-m-d');
		$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
		
		if($urutanchecker == null){
			$urutanchecker=0;
		}
		
	
        foreach($data_menu as $rows){
		
			//DAPATKAN JUMLAH YANG ADA
			if($data_jmlout[$i] > 0)
			{
			
			//UNTUK NAMA MENU DISIMPAN KEDALAM BINDING PARAMETER
			$query="insert into TORDERCHECKER values('$rows->KODELOKASI','$rows->KODETRANS','$rows->TGLOMZET',
					'$rows->KODEMENURECIPE',?,$data_jmlout[$i],$rows->URUTAN,($urutanchecker+1),'$rows->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
			$this->db->query($query, $rows->KETERANGAN);
			
			$urutanchecker++;
			}
			
			$i++;
		}
		return true;
    }
	
	public function simpanDetailFinish($data_menu, $data_jmlout, $data_sisa, $data_macaddress,$data_username)
    {

		$i = 0;
		
		//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
		$sql = "SELECT URUTANCHECKER FROM TORDERCHECKER
			ORDER BY URUTANCHECKER DESC";
			
		$urutanchecker = $this->db->query($sql)->row()->URUTANCHECKER;
		
		if($urutanchecker == null){
			$urutanchecker=0;
		}
		
		//UNTUK AMBIL JAM DISERVER
		$sqlJam = "SELECT CURRENT_TIME as JAMNOW FROM TORDERDTL";
		
		$tglFinish = date('Y-m-d');
		$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
		
        foreach($data_menu as $rows){
			
			$jml_minus = $data_jmlout[$i];
			
			//----------------------------------------------------------------------------
			
			$queryDataChecker="select * from torderchecker
					where KODELOKASI = '$rows->KODELOKASI' 
					  and KODETRANS = '$rows->KODETRANS' 
					  and TGLOMZET = '$rows->TGLOMZET'
					  and KODEMENURECIPE = '$rows->KODEMENURECIPE'
					  and URUTAN = $rows->URUTAN
					  ";
					  
			$dataChecker = $this->db->query($queryDataChecker)->row();
			
			 $query="insert into TORDERCHECKER values('$dataChecker->KODELOKASI','$dataChecker->KODETRANS','$dataChecker->TGLOMZET',
					 '$dataChecker->KODEMENURECIPE', ?,($jml_minus*-1),$dataChecker->URUTAN,($urutanchecker+1),'$dataChecker->JAMORDER','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
			 $this->db->query($query, $rows->NAMAMENURECIPE);
			
			$urutanchecker++;
			
			$i++;
		}
		return true;
    }
	
	public function simpanDetailKitchen($data_kodemenu, $data_jmlout, $data_sisa,$data_macaddress,$data_username)
    { $i = 0;
		
		//UNTUK AMBIL JAM DISERVER
		$sqlJam = "SELECT CURRENT_TIME as JAMNOW FROM TORDERDTL";
		
		$tglFinish = date('Y-m-d');
		$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
		$kodemenu = "";
  
		if($data_jmlout > 0)
		{
			//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
			$sqlKitchen = "SELECT A.* FROM TORDERDTL A
							INNER JOIN  TORDER A1 on A1.KODETRANS = A.KODETRANS and A1.TGLOMZET = A.TGLOMZET
							INNER JOIN CLOSINGDATE C ON C.TGLOMZET=A1.TGLOMZET
						WHERE C.USERCLOSE = '' and A.KODEMENURECIPE = '".$data_kodemenu."' 
						and a.SPESIFIKASI = 'REGULAR'
						and not exists (select 1 from torderkitchen h where h.kodetrans = a.kodetrans and h.tglomzet = a.tglomzet and h.kodelokasi = a.kodelokasi and a.kodemenurecipe = h.kodemenurecipe 
						HAVING SUM(h.jumlah) = a.jml )
						ORDER BY A.KODETRANS ASC";
			$dataKitchen = $this->db->query($sqlKitchen)->result();
		
			$cekDone = false;
			foreach($dataKitchen as $rows_kitchen){

				//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
				$sql = "SELECT URUTANKITCHEN FROM TORDERKITCHEN 
					ORDER BY URUTANKITCHEN DESC";
					
				$urutankitchen = $this->db->query($sql)->row()->URUTANKITCHEN;
				
				if($urutankitchen == null){
					$urutankitchen=0;
				}
				
				$sqlOrderKitchen = "SELECT SUM(JUMLAH) FROM TORDERKITCHEN
						WHERE  KODEMENURECIPE = '".$rows_kitchen->KODEMENURECIPE."' 
						and TGLOMZET = '".$rows_kitchen->TGLOMZET."'
						and URUTAN = '".$rows_kitchen->URUTAN."'
						and KODETRANS = '".$rows_kitchen->KODETRANS."'
						and KODELOKASI = '".$rows_kitchen->KODELOKASI."'
						GROUP BY KODETRANS";
						
				$dataOrderKitchen = $this->db->query($sqlOrderKitchen)->row();
			
				$jumlahGabungan = ($rows_kitchen->JML - $rows_kitchen->JMLVOID - $dataOrderKitchen->JUMLAH );
				if($cekDone == false)
				{
					if($jumlahGabungan >= $data_jmlout){ // JIKA TRANSAKSI MEMILIKI MENU YG CKUP
					 $jumlahMenu = $data_jmlout;
						 if($jumlahMenu != 0)
						 {
							  $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
							 '$rows_kitchen->KODEMENURECIPE', ?,$jumlahMenu,$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
							 $this->db->query($query, $rows_kitchen->KETERANGAN);
							 $cekDone = true;
							 $data_jmlout = $data_jmlout - $jumlahMenu;
						 }
					}
					else // JIKA TRANSAKSI MEMILIKI MENU YANG TIDAK CKUP
					{
						$jumlahMenu = $jumlahGabungan;
						if($jumlahMenu != 0)
						{
							 $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
								'$rows_kitchen->KODEMENURECIPE', ?,$jumlahMenu,$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
							$this->db->query($query, $rows_kitchen->KETERANGAN);
							$data_jmlout = $data_jmlout - $jumlahMenu;
						}
					}
					$urutankitchen++;
					
					
				}
				
			}
		}
			
		return true;
    }
	
	public function simpanDetailKitchenFinish($data_kodemenu, $data_jmlout, $data_sisa,$data_macaddress,$data_username)
    {
		$i = 0;
		
		//UNTUK AMBIL JAM DISERVER
		$sqlJam = "SELECT CURRENT_TIME as JAMNOW, A.TGLOMZET,A.KODELOKASI FROM TORDERDTL A
		INNER JOIN CLOSINGDATE C ON C.TGLOMZET=A.TGLOMZET
		WHERE C.USERCLOSE = ''";
		
		$tglFinish = date('Y-m-d');
		$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
		$tglOmzet = $this->db->query($sqlJam)->row()->TGLOMZET;
		$kodeLokasi = $this->db->query($sqlJam)->row()->KODELOKASI;
		$kodemenu = "";
  
		if($data_jmlout > 0)
		{
			//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
			$sqlKitchen = "SELECT A.* FROM TORDERKITCHEN A
						WHERE A.KODEMENURECIPE = '".$data_kodemenu."' 
						AND A.TGLOMZET = '".$tglOmzet."' 
						AND A.KODELOKASI = '".$kodeLokasi."'
						ORDER BY A.KODETRANS DESC";
			$dataKitchen = $this->db->query($sqlKitchen)->result();
			$cekDone = false;
			foreach($dataKitchen as $rows_kitchen){

				//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
				$sql = "SELECT URUTANKITCHEN FROM TORDERKITCHEN 
					ORDER BY URUTANKITCHEN DESC";
					
				$urutankitchen = $this->db->query($sql)->row()->URUTANKITCHEN;
				
				if($urutankitchen == null){
					$urutankitchen=0;
				}
				
				$jumlahGabungan = ($rows_kitchen->JUMLAH);
				if($cekDone == false)
				{	
					if($jumlahGabungan >= $data_jmlout){ // JIKA TRANSAKSI MEMILIKI MENU YG CKUP
						 $jumlahMenu = $data_jmlout;
						 $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
							 '$rows_kitchen->KODEMENURECIPE', ?,($jumlahMenu * -1),$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMORDER','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
						 $this->db->query($query,$rows_kitchen->NAMAMENURECIPE);
						 
						 $cekDone = true;
						 $data_jmlout = $data_jmlout - $jumlahMenu;
					}
					else // JIKA TRANSAKSI MEMILIKI MENU YANG TIDAK CKUP
					{
						$jumlahMenu = $jumlahGabungan;
						
						 $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
							'$rows_kitchen->KODEMENURECIPE',?,($jumlahMenu * -1),$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMORDER','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
						$this->db->query($query,$rows_kitchen->NAMAMENURECIPE);
						$data_jmlout = $data_jmlout - $jumlahMenu;
					}
					
					$urutankitchen++;
				}
				
			}
		}
			
		return true;
    }
	
	public function simpanOrderAll($data_macaddress,$data_username)
    {
        $sql = "SELECT A. TGLOMZET, A.KODETRANS, A.NOMORMEJA, A.JUMLAHORANG, A.WAITER, A.KODELOKASI,A.JAMTRANS
                FROM TORDER A INNER JOIN TORDERDTL B ON A.KODETRANS=B.KODETRANS AND A.TGLOMZET=B.TGLOMZET
                     LEFT JOIN CLOSINGDATE C ON C.TGLOMZET=A.TGLOMZET
                WHERE C.USERCLOSE = ''
                GROUP BY A.TGLOMZET, A.JAMTRANS, A.KODETRANS, A.NOMORMEJA, A.JUMLAHORANG, A.WAITER, A.KODELOKASI
				ORDER BY A.TGLOMZET DESC, A.JAMTRANS DESC, A.NOMORMEJA";
                       
        $query = $this->db->query($sql)->result();
		
		foreach($query as $rowsHeader)
		{
			
			$sql = "SELECT a.*, CAST(a.JML-a.JMLVOID AS INTEGER) AS JML2, CAST(a.JML-a.JMLVOID AS INTEGER) AS SISA
                FROM TORDERDTL a
				INNER JOIN MMENURECIPE B ON A.KODEMENURECIPE = B.KODE
                WHERE A.KODELOKASI = '".$rowsHeader->KODELOKASI."' AND A.TGLOMZET='".$rowsHeader->TGLOMZET."' AND A.KODETRANS = '".$rowsHeader->KODETRANS."' AND A.JENIS='' AND (a.JML-a.JMLVOID) > 0 AND a.SPESIFIKASI = 'REGULAR' AND B.CHECKER = 1
				ORDER BY a.TGLTRANS,a.JAMTRANS";
        
			$query = $this->db->query($sql)->result();
			;
			foreach($query as $rows){
				
				 $sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as SISA
					FROM TORDERCHECKER A
					WHERE A.TGLOMZET='".$rowsHeader->TGLOMZET."' AND A.KODETRANS = '".$rowsHeader->KODETRANS."' AND A.KODELOKASI = '".$rows->KODELOKASI."'
					AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."' AND A.URUTAN = $rows->URUTAN ";
						   
				$sisa = $this->db->query($sql)->row();
				
				if($sisa->SISA == "")
				{
					$sisa->SISA 	= 0;
				}
				$rows->SISA 		= $rows->SISA-$sisa->SISA;
				
				$i = 0;
		
				$tglomzet = $rows->TGLOMZET;
				$kodelokasi = $rows->KODELOKASI;
				$kodetrans = $rows->KODETRANS;
				
				//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
				$sql = "SELECT URUTANCHECKER,CURRENT_TIME as JAMNOW FROM TORDERCHECKER
					ORDER BY URUTANCHECKER DESC";
					
				$urutanchecker = $this->db->query($sql)->row()->URUTANCHECKER;
				
				//UNTUK AMBIL JAM DISERVER
				$sqlJam = "SELECT CURRENT_TIME as JAMNOW FROM TORDERDTL";
				
				$tglFinish = date('Y-m-d');
				$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
				
				if($urutanchecker == null){
					$urutanchecker=0;
				}
				
				//DAPATKAN JUMLAH YANG ADA
				if($rows->SISA > 0)
				{
					$query ="insert into TORDERCHECKER values('$rows->KODELOKASI','$rows->KODETRANS','$rows->TGLOMZET',
							'$rows->KODEMENURECIPE',?,$rows->SISA,$rows->URUTAN,($urutanchecker+1),'$rows->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
					$this->db->query($query, $rows->KETERANGAN);
					
					
					$urutanchecker++;
				}
			}
		}
		//CEK NOL TIDAK KELUAR DI FRONT
		
        return true;
    }
	public function simpanKitchenAll($data_macaddress,$data_username)
    {
		// KURANG PENGECEKAN TIDAK ADA DI ORDERKITCHEN KODENYA
		 $sql = "SELECT B.TGLOMZET,B.KODELOKASI,A.NAMA,B.KODEMENURECIPE, CAST(SUM(B.JML - B.JMLVOID) AS INTEGER) as JML, CAST(SUM(B.JML - B.JMLVOID) AS INTEGER) as SISA 
			FROM MMENURECIPE A
				INNER JOIN TORDERDTL B ON A.KODE=B.KODEMENURECIPE 
				INNER JOIN TORDER C ON C.KODETRANS = B.KODETRANS AND C.TGLOMZET=B.TGLOMZET
				INNER JOIN CLOSINGDATE D ON C.TGLOMZET=D.TGLOMZET
		   WHERE a.PREPARE = 1 and B.SPESIFIKASI = 'REGULAR' AND D.USERCLOSE = '' AND (b.JML-b.JMLVOID) > 0
		   GROUP BY B.TGLOMZET,B.KODELOKASI,a.NAMA,b.KODEMENURECIPE 
		   ORDER BY a.NAMA ASC
                ";//AND A.KODE = '1996-0001'       
                       
        $query = $this->db->query($sql)->result();
		$oldnama = "";
		foreach($query as $rows){
			
			$sql = "SELECT CAST(SUM(A.JUMLAH) AS INTEGER) as JML
				FROM TORDERKITCHEN A
				WHERE A.TGLOMZET='".$rows->TGLOMZET."' AND A.KODELOKASI = '".$rows->KODELOKASI."'
				AND A.KODEMENURECIPE = '".$rows->KODEMENURECIPE."'";
					   
			$jml = $this->db->query($sql)->row();
			
			if($jml->JML == "")
			{
				$jml->JML 	= 0;
			}

			$rows->SISA 	= $rows->SISA-$jml->JML;
				
		}
		
		foreach($query as $rowsHeader)
		{
			$i = 0;
		
			//UNTUK AMBIL JAM DISERVER
			$sqlJam = "SELECT CURRENT_TIME as JAMNOW FROM TORDERDTL";
			
			$tglFinish = date('Y-m-d');
			$jamFinish = $this->db->query($sqlJam)->row()->JAMNOW;
			$kodemenu = "";
	  
			if($rowsHeader->SISA > 0)
			{
				//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
				$sqlKitchen = "SELECT A.* FROM TORDERDTL A
								INNER JOIN  TORDER A1 on A1.KODETRANS = A.KODETRANS and A1.TGLOMZET = A.TGLOMZET
								INNER JOIN CLOSINGDATE C ON C.TGLOMZET=A1.TGLOMZET
							WHERE C.USERCLOSE = '' and A.KODEMENURECIPE = '".$rowsHeader->KODEMENURECIPE."'
							and not exists (select 1 from torderkitchen h where h.kodetrans = a.kodetrans and h.tglomzet = a.tglomzet and h.kodelokasi = a.kodelokasi and a.kodemenurecipe = h.kodemenurecipe
							HAVING SUM(h.jumlah) = a.jml) 							
							and a.SPESIFIKASI = 'REGULAR'
							ORDER BY A.KODETRANS ASC";
				$dataKitchen = $this->db->query($sqlKitchen)->result();
			
				$cekDone = false;
				foreach($dataKitchen as $rows_kitchen){

					//BELUM SESUAI URUT DIA AMBIL MESTI SESUDAH
					$sql = "SELECT URUTANKITCHEN FROM TORDERKITCHEN 
						ORDER BY URUTANKITCHEN DESC";
						
					$urutankitchen = $this->db->query($sql)->row()->URUTANKITCHEN;
					
					if($urutankitchen == null){
						$urutankitchen=0;
					}
					
					$sqlOrderKitchen = "SELECT SUM(JUMLAH) FROM TORDERKITCHEN
							WHERE  KODEMENURECIPE = '".$rows_kitchen->KODEMENURECIPE."' 
							and TGLOMZET = '".$rows_kitchen->TGLOMZET."'
							and URUTAN = '".$rows_kitchen->URUTAN."'
							and KODETRANS = '".$rows_kitchen->KODETRANS."'
							and KODELOKASI = '".$rows_kitchen->KODELOKASI."'
							GROUP BY KODETRANS";
							
					$dataOrderKitchen = $this->db->query($sqlOrderKitchen)->row();
				
					$jumlahGabungan = ($rows_kitchen->JML - $rows_kitchen->JMLVOID - $dataOrderKitchen->JUMLAH);
					if($cekDone == false)
					{
						if($jumlahGabungan >= $rowsHeader->SISA){ // JIKA TRANSAKSI MEMILIKI MENU YG CKUP
						 $jumlahMenu = $rowsHeader->SISA;
							 if($jumlahMenu != 0)
							 {
								  $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
								 '$rows_kitchen->KODEMENURECIPE', ?,$jumlahMenu,$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
								 $this->db->query($query, $rows_kitchen->KETERANGAN);
								 $cekDone = true;
								 $rowsHeader->SISA = $rowsHeader->SISA - $jumlahMenu;
							 }
						}
						else // JIKA TRANSAKSI MEMILIKI MENU YANG TIDAK CKUP
						{
							$jumlahMenu = $jumlahGabungan;
							if($jumlahMenu != 0)
							{
								 $query="insert into TORDERKITCHEN values('$rows_kitchen->KODELOKASI','$rows_kitchen->KODETRANS','$rows_kitchen->TGLOMZET',
									'$rows_kitchen->KODEMENURECIPE', ?,$jumlahMenu,$rows_kitchen->URUTAN,($urutankitchen+1),'$rows_kitchen->JAMTRANS','$jamFinish','$tglFinish','$data_username','I',',$data_macaddress')";
								$this->db->query($query, $rows_kitchen->KETERANGAN);
								$rowsHeader->SISA = $rowsHeader->SISA - $jumlahMenu;
							}
						}
						$urutankitchen++;
						
						
					}
					
				}
			}
			
			
		}
		//CEK NOL TIDAK KELUAR DI FRONT
		
        return true;
    }

}
