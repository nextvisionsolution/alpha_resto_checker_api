<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	/**
	 * Menampilkan data dalam bentuk json
	 *
	 * @param array $data
	 * @return void
	 */
	protected function responseJson($data)
	{
		$this->output->set_content_type('application/json')
						->set_output(json_encode($data));
	}

}
