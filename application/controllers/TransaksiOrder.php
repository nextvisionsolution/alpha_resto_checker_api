<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiOrder extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
		
		header("Access-Control-Allow-Origin: *");
		//header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, HEAD");
        $this->load->model('model_transaksi_order');
    }

    /**
     * Menampilkan daftar record pada TORDER dimana TGLOMZET sesuai pada TGLOMZET di tabel
     * CLOSINGDATE dengan USERCLOSE kosong
     *
     * @return void
     */
    public function index()
    {
        $data = $this->model_transaksi_order->getTransaksiBelumDitutup();

        $this->responseJson($data);
    
	}
	
	public function login()
    {
		
        $user  = $_POST['USERNAME'];
        $pass = $_POST['PASSWORD'];
        $data = $this->model_transaksi_order->getUser($user, $pass);
		echo $data;
    }
    
    /**
     * Menampilkan detail transaksi order
     *
     * @return void
     */
	
	function push_notification_android(){

		//API URL of FCM
		$url = 'https://fcm.googleapis.com/fcm/send';

		/*api_key available in:
		Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    
		$api_key = 'AAAAEUkOi3s:APA91bE6bX1ZWxOoNY7Y9giiFC1uyl0Mobv2yj3U_yjp5iyxFeyDMS4-Rsjcu6DEhQ3AA7KfrlYnDUgZmaOtq4KYf1NDlRVP7FLZy6Vgl1szok_OmEGkoF_DLqOKZujipMlDWGja_4Hq';
					
		$fields = array (
			'registration_ids' => array (
					"eLWdK0JpVJI:APA91bG73iM-YieS4Wm1e6AuCijbAvj2MAQPj6ut-mMQ44eZd2XPPUv05C4H6LgDJscCBXsklGpB-HyBE6iNwQdkRczJkMqIcu_ExRR8tI_hB8BDAYnoGex-C7743bCgXkIurcW0xdce"
			),
			'notification' => array (
					"title" => 'a',
					"body"  => 'b',
			)
		);

		//header includes Content type and api key
		$headers = array(
			'Content-Type:application/json',
			'Authorization:key='.$api_key
		);
					
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
		echo $result;
	}
	
    public function daftarBill()
    {
        $data = $this->model_transaksi_order->getDaftarBill();
		
        $this->responseJson($data);
    }
	
	public function daftarMenu()
    {
        $data = $this->model_transaksi_order->getDaftarMenu();

        $this->responseJson($data);
    }
	
	public function daftarKitchen()
    {
        $data = $this->model_transaksi_order->getDaftarKitchen();

        $this->responseJson($data);
    }
	
	public function daftarFinish()
    {
        $data = $this->model_transaksi_order->getDaftarFinish();

        $this->responseJson($data);
    }
	
	public function daftarKitchenFinish()
    {
        $data = $this->model_transaksi_order->getDaftarKitchenFinish();

        $this->responseJson($data);
    }
	
    public function daftarDetailMenu()
    {
        $tglomzet  = $_POST['tglomzet'];
        $kodetrans = $_POST['kodetrans'];
        $data = $this->model_transaksi_order->getDaftarDetailMenu($tglomzet, $kodetrans);

        $this->responseJson($data);
    }

    public function detail()
    {
        $data = $this->model_transaksi_order->getDetail();

        $this->responseJson($data);
    }

    public function detailOrder()
    {
        $tglomzet  = $_POST['TGLOMZET'];
        $kodetrans = $_POST['KODETRANS'];
        $jenis = $_POST['JENIS'];
        $data = $this->model_transaksi_order->getDaftarDetailMenu($tglomzet, $kodetrans);

        $this->responseJson($data);
    }
	
	public function detailFinish()
    {
        $tglomzet  = $_POST['TGLOMZET'];
        $kodetrans = $_POST['KODETRANS'];
        $jenis = $_POST['JENIS'];
        $data = $this->model_transaksi_order->getDaftarDetailFinish($tglomzet, $kodetrans);

        $this->responseJson($data);
    }
	
	public function detailOrderTambahan()
    {
		$kodemenu 		= $_POST['KODEMENU'];
        $data = $this->model_transaksi_order->getDaftarDetailTambahan($kodemenu);

        $this->responseJson($data);
    }
	
	public function simpanDetailOrder()
    {
		$data_jmlout = json_decode($_POST['jmlout']);
		$data_menu = json_decode($_POST['menu']);
		$data_sisa = json_decode($_POST['sisa']);
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanDetailOrder($data_menu, $data_jmlout,$data_sisa,$data_macaddress,$data_username);
		$this->responseJson($data);
    }
	
	public function simpanDetailMenu()
    {
		$data_jmlout = json_decode($_POST['jmlout']);
		$data_table = json_decode($_POST['table']);
		$data_sisa = json_decode($_POST['sisa']);
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
		$data = $this->model_transaksi_order->simpanDetailOrder($data_table, $data_jmlout,$data_sisa,$data_macaddress,$data_username);
		$this->responseJson($data);
    }
	
	public function simpanDetailFinish()
    {
		$data_jmlout = json_decode($_POST['jmlout']);
		$data_menu = json_decode($_POST['menu']);
		$data_sisa = json_decode($_POST['sisa']);
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanDetailFinish($data_menu, $data_jmlout,$data_sisa,$data_macaddress,$data_username);
		$this->responseJson($data);
    }
	
	public function simpanDetailKitchen()
    {
		$data_jmlout = $_POST['jmlout'];
		$data_menu = $_POST['kodemenu'];
		$data_sisa = $_POST['sisa'];
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanDetailKitchen($data_menu, $data_jmlout,$data_sisa,$data_macaddress,$data_username);
		$this->responseJson($data);
    }
	
	public function simpanDetailKitchenFinish()
    {
		$data_jmlout = $_POST['jmlout'];
		$data_menu = $_POST['kodemenu'];
		$data_sisa = $_POST['sisa'];
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanDetailKitchenFinish($data_menu, $data_jmlout,$data_sisa,$data_macaddress,$data_username);
		$this->responseJson($data);
    }
	
	public function simpanOrderAll()
    {
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanOrderAll($data_macaddress,$data_username);

        $this->responseJson($data);
    }
	
	public function simpanKitchenAll()
    {
		$data_macaddress = $_POST['macaddress'];
		$data_username = $_POST['username'];
        $data = $this->model_transaksi_order->simpanKitchenAll($data_macaddress,$data_username);

        $this->responseJson($data);
    }
}
